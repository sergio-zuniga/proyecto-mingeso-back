FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/*.jar proyecto_mingeso.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/proyecto_mingeso.jar"]
package mingeso.Controlador;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import javax.validation.Valid;

import mingeso.Dto.modelo.AnuncioDTO;
import mingeso.modelo.*;
import mingeso.payload.request.ComentarioRequest;
import mingeso.payload.response.RankingResPonse;
import mingeso.repositorio.AnuncioRepository;
import mingeso.repositorio.CategoriaRepository;
import mingeso.repositorio.ComentarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import mingeso.payload.request.AnuncioRequest;
import mingeso.payload.response.MessageResponse;

import mingeso.repositorio.UsuarioRepository;
import mingeso.seguridad.jwt.JwtUtils;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/anuncios")
public class AnuncioController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AnuncioRepository anuncioRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    CategoriaRepository categoriaRepository;

    @Autowired
    ComentarioRepository comentarioRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    /*
    Método que realiza una publicación de un anuncio, recibe los datos por el cuerpo del post y luego de realizar
    algunas verificaciones lo guarda en la base de datos
     */
    @PostMapping("/publicar")
    public ResponseEntity<?> publicarAnuncio(@Valid @RequestBody AnuncioRequest anuncioRequest) { //el " ResponseEntity<?> " es un Code Smell detectado como critico
        if (usuarioRepository.existsById(anuncioRequest.getId_usuario()))
        {


            Set<String> strCategorias = anuncioRequest.getCategorias();
            Set<Categoria> categorias = new HashSet<>();

            if (strCategorias == null) {
                    //este es un Code Smell porque esta funcion podria facilmente cambiarse por el booleano opuesto

            } else {
                strCategorias.forEach(categoria -> {
                    if (categoriaRepository.existsByNombre(categoria))
                    {
                        Categoria categoriaAnuncio = categoriaRepository.getFirstByNombre(categoria);
                        categorias.add(categoriaAnuncio);
                    }
                    else
                    {
                        Categoria categoriaAnuncio = new Categoria(categoria);
                        categoriaRepository.save(categoriaAnuncio);
                        categoriaAnuncio = categoriaRepository.getFirstByNombre(categoria);

                        categorias.add(categoriaAnuncio);
                    }

                });
            }


            Anuncio anuncio = new Anuncio(anuncioRequest.getTitulo(),anuncioRequest.getCorreo(),anuncioRequest.getTelefono()
            , anuncioRequest.getDescripcion(),anuncioRequest.getPrecio(),usuarioRepository.getFirstById(anuncioRequest.getId_usuario())
            , categorias);

            anuncioRepository.save(anuncio);
        }
        else
        {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: No existe usuario"));
        }


        return ResponseEntity.ok(new MessageResponse("Anuncio creado"));
    }




    @PostMapping("/comentar")
    public ResponseEntity<?> comentarAnuncio(@Valid @RequestBody ComentarioRequest comentarioRequest) {

        if (usuarioRepository.existsById(comentarioRequest.getId_usuario()))
        {
            if (anuncioRepository.existsById(comentarioRequest.getId_anuncio()))
            {
                Usuario usuario = usuarioRepository.getFirstById(comentarioRequest.getId_usuario());
                Anuncio anuncio = anuncioRepository.getOne(comentarioRequest.getId_anuncio());
                Comentario comentario = new Comentario(comentarioRequest.getCuerpo(), usuario, anuncio);
                comentarioRepository.save(comentario);
            }
            else
            {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: No existe anuncio"));
            }

        }
        else
        {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: No existe usuario"));
        }


        return ResponseEntity.ok(new MessageResponse("Comentario creado"));
    }



    @GetMapping(path="/obtener-ranking")
    public @ResponseBody List<RankingResPonse> getAllRanking(@RequestParam(required = true) String comuna) {


        return anuncioRepository.obtenerRanking(comuna);
    }



}

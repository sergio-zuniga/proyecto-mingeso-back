package mingeso.Controlador;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import mingeso.modelo.Comuna;
import mingeso.modelo.Usuario;
import mingeso.repositorio.ComunaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mingeso.modelo.NombreTipo;
import mingeso.modelo.Tipo;

import mingeso.payload.request.LoginRequest;
import mingeso.payload.request.SignupRequest;
import mingeso.payload.response.JwtResponse;
import mingeso.payload.response.MessageResponse;

import mingeso.repositorio.TipoRepository;
import mingeso.repositorio.UsuarioRepository;
import mingeso.seguridad.jwt.JwtUtils;
import mingeso.seguridad.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/autenticacion")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	TipoRepository tipoRepository;

	@Autowired
	ComunaRepository comunaRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/iniciar")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getCorreo(), loginRequest.getPass()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getNombre(),
												 userDetails.getUsername(),
												 roles));
	}

	@PostMapping("/registrar")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

		if (usuarioRepository.existsByCorreo(signUpRequest.getCorreo())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Correo en uso"));
		}


		Usuario usuario = new Usuario(signUpRequest.getNombre(),
							 signUpRequest.getCorreo(),
							 encoder.encode(signUpRequest.getPass()));

		Set<String> strRoles = signUpRequest.getTipo();
		Set<Tipo> tipos = new HashSet<>();

		if (strRoles == null) {
			Tipo userTipo = tipoRepository.findByName(NombreTipo.TIPO_VECINO);
			tipos.add(userTipo);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "representante":
					Tipo adminTipo = tipoRepository.findByName(NombreTipo.TIPO_REPRESENTANTE);
					tipos.add(adminTipo);

					break;
				case "municipalidad":
					Tipo modTipo = tipoRepository.findByName(NombreTipo.TIPO_MUNICIPALIDAD);
					tipos.add(modTipo);

					break;
				default:
					Tipo userTipo = tipoRepository.findByName(NombreTipo.TIPO_VECINO);
					tipos.add(userTipo);
				}
			});
		}

		usuario.setTipos(tipos);

		if (comunaRepository.existsByNombre(signUpRequest.getComuna()))
		{
			Comuna comuna = comunaRepository.getFirstByNombre(signUpRequest.getComuna());

		}
		else
		{
			Comuna comuna = new Comuna(signUpRequest.getComuna());
			comunaRepository.save(comuna);
			comuna = comunaRepository.getFirstByNombre(signUpRequest.getComuna());
			usuario.setComuna(comuna);

		}


		usuarioRepository.save(usuario);

		return ResponseEntity.ok(new MessageResponse("Usuario registrado"));
	}
}

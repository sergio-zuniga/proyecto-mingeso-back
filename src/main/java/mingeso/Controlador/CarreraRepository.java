package mingeso.Controlador;

import mingeso.modelo.Carrera;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository("carrera")
public interface CarreraRepository extends CrudRepository<Carrera, Long> {
    //public Carrera findByIdAndAlumnos


}


package mingeso.Controlador;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://209.97.147.143:8081", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}
	
	@GetMapping("/vecino")
	@PreAuthorize("hasRole('VECINO') or hasRole('MUNICIPALIDAD') or hasRole('ADMIN')")
	public String userAccess() {
		return "Tablero de voluntario.";
	}

	@GetMapping("/muni")
	@PreAuthorize("hasRole('MUNICIPALIDAD')")
	public String moderatorAccess() {
		return "Tablero de Municipalidad.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Tablero de Admin.";
	}
}

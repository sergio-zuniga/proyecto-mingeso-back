package mingeso.Dto.controlador;

import mingeso.Dto.modelo.AnuncioDTO;
import mingeso.Dto.modelo.ComentarioDTO;
import mingeso.modelo.Anuncio;
import mingeso.modelo.Categoria;
import mingeso.modelo.Comentario;
import mingeso.repositorio.AnuncioRepository;
import mingeso.repositorio.CategoriaRepository;
import mingeso.repositorio.ComentarioRepository;
import mingeso.repositorio.UsuarioRepository;
import mingeso.seguridad.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/anuncios")
public class AnuncioControllerDTO {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AnuncioRepository anuncioRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    CategoriaRepository categoriaRepository;

    @Autowired
    ComentarioRepository comentarioRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    @GetMapping(path="/all_anuncio")
    public @ResponseBody Iterable<AnuncioDTO> getAllAnuncios() {


        List<Anuncio> anuncios = anuncioRepository.findAll();
        List<AnuncioDTO> retorno = new ArrayList<>();

        anuncios.forEach(anuncio -> {


            AnuncioDTO dto = new AnuncioDTO();
            dto.setId(anuncio.getId());
            dto.setTitulo(anuncio.getTitulo());
            dto.setCorreo(anuncio.getCorreo());
            dto.setTelefono(anuncio.getTelefono());
            dto.setDescripcion(anuncio.getDescripcion());
            dto.setFecha(anuncio.getFecha());
            dto.setCategorias(anuncio.getCategorias());
            dto.setNombre(anuncio.getUsuario().getNombre());
            dto.setPrecio(anuncio.getPrecio());
            retorno.add(dto);
        });

        return retorno;

    }

    @GetMapping(path="/por_categoria")
    public @ResponseBody Iterable<AnuncioDTO> getAllAnunciosByCategoria(@RequestParam(required = true) String categoria) {

        Categoria cat = categoriaRepository.getFirstByNombre(categoria);
        Iterable<Anuncio> anuncios = anuncioRepository.getAllByCategorias(cat);

        List<AnuncioDTO> retorno = new ArrayList<>();

        anuncios.forEach(anuncio -> {


            AnuncioDTO dto = new AnuncioDTO();
            dto.setId(anuncio.getId());
            dto.setTitulo(anuncio.getTitulo());
            dto.setCorreo(anuncio.getCorreo());
            dto.setTelefono(anuncio.getTelefono());
            dto.setDescripcion(anuncio.getDescripcion());
            dto.setFecha(anuncio.getFecha());
            dto.setCategorias(anuncio.getCategorias());
            dto.setNombre(anuncio.getUsuario().getNombre());
            dto.setPrecio(anuncio.getPrecio());
            retorno.add(dto);
        });

        return retorno;
    }


    @GetMapping(path="/comentarios")
    public @ResponseBody Iterable<ComentarioDTO> getAllAnunciosByCategoria(@RequestParam(required = true) Long anuncio) {

        Anuncio anuncio1 = anuncioRepository.getOne(anuncio);
        Iterable<Comentario> comentarios = comentarioRepository.getAllByAnuncio(anuncio1);

        List<ComentarioDTO> retorno = new ArrayList<>();

        comentarios.forEach(comentario -> {


            ComentarioDTO dto = new ComentarioDTO(comentario.getId(),comentario.getCuerpo(),comentario.getFecha(),comentario.getUsuario().getNombre());

            retorno.add(dto);
        });

        return retorno;
    }

}
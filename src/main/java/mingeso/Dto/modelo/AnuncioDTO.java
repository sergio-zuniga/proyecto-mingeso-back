package mingeso.Dto.modelo;

import mingeso.modelo.Categoria;

import java.util.HashSet;
import java.util.Set;

public class AnuncioDTO {

    private long id;

    private String titulo;

    private String correo;

    private String telefono;

    private String descripcion;

    private String fecha;

    private Set<Categoria> categorias = new HashSet<>();

    private String nombre;

    private float precio;

    //Constructor especial de la clase
    public AnuncioDTO(String titulo,String correo, String telefono, String descripcion, String fecha, String nombre, float precio){
        this.titulo=titulo;
        this.telefono=telefono;
        this.correo=correo;
        this.descripcion=descripcion;
        this.fecha=fecha;
        this.precio=precio;
        this.nombre=nombre;
        this.categorias=new HashSet<>();
    }
    //constructor por defecto de la clase
    public AnuncioDTO(){

    }

    //metodos de la clase:
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Set<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(Set<Categoria> categorias) {
        this.categorias = categorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}

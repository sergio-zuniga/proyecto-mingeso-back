package mingeso.Dto.modelo;

import mingeso.modelo.Categoria;

import java.util.HashSet;
import java.util.Set;

public class ComentarioDTO {

    private long id;

    private String cuerpo;

    private String fecha;

    private String usuario;

    //Constructor especial de la clase

    public ComentarioDTO(long id, String cuerpo, String fecha, String usuario) {
        this.id = id;
        this.cuerpo = cuerpo;
        this.fecha = fecha;
        this.usuario = usuario;
    }

    //constructor por defecto de la clase
    public ComentarioDTO(){

    }

    //metodos de la clase:


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}

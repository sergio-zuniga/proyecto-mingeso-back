package mingeso.Dto.modelo;

public class UsuarioDTO {


    private long id;

    private String nombre;

    private String correo;

    //constructor de la clase con 2 parametros, correspondiente al:
    //nombre y correo del usuario
    public UsuarioDTO(String nombre, String correo){
        this.nombre=nombre;
        this.correo=correo;
    }
    //constructor por defecto
    public UsuarioDTO(){}
    //funcion getter de correoDTO
    public String getCorreo() {
        return correo;
    }
    //funcion getter del nombre del usuarioDTO
    public String getNombre() {
        return nombre;
    }
    //funcion getter del id del usuario
    public long getId() {
        return id;
    }
}

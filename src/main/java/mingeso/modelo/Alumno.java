package mingeso.modelo;

import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Column;

import javax.persistence.*;


@Entity
@Table(name = "alumno")
public class Alumno {
    @Id
    @Column (name = "rut", unique = true, nullable = false)
    private String rut;

    private String nombre;

    private String nacimiento;

    public Alumno(String rut, String nombre, String nacimiento, Carrera carrera)
    {
        this.rut = rut;
        this.nombre = nombre;
        this.nacimiento = nacimiento;
        this.carrera = carrera;
    }

    public Alumno()
    {

    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }



    @ManyToOne
    @JoinColumn(name = "id_carrera")
    private Carrera carrera;


    public Carrera getCarrera() {
        return carrera;
    }
}

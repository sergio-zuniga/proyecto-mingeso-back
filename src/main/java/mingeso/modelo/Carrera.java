package mingeso.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Column;

import javax.persistence.*;

import java.util.List;



@Entity
@Table(name="carrera")
public class Carrera {

    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    private String nombre;

    public Carrera(String nombre)
    {
        this.nombre = nombre;
    }

    public Carrera()
    {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    @OneToMany(mappedBy = "carrera")
    private List<Alumno> alumnos;


}

package mingeso.modelo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(	name = "categoria")
public class Categoria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Column(unique=true)
    @Size(max = 100)
    private String nombre;



    public Categoria() {
    }

    public Categoria(String nombre) {
        this.nombre = nombre;

    }
    public long getId(){return id;}

    public void setId(Long id){ this.id=id; }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
package mingeso.modelo;

public class Intermedia {
    private String rut;
    private Long id;

    public Intermedia(String rut, long id){
        this.rut=rut;
        this.id=id;
    }
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

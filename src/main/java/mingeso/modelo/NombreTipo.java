package mingeso.modelo;

public enum NombreTipo {
    TIPO_VECINO,
    TIPO_MUNICIPALIDAD,
    TIPO_REPRESENTANTE
}

package mingeso.modelo;

import javax.persistence.*;

@Entity
@Table(name = "tipo")
public class Tipo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private NombreTipo name;

    public Tipo() {

    }

    public Tipo(NombreTipo name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NombreTipo getName() {
        return name;
    }

    public void setName(NombreTipo name) {
        this.name = name;
    }
}
package mingeso.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class ComentarioRequest {

    private long id_anuncio;

    private long id_usuario;

    @NotBlank
    @Size(max = 500)
    private String cuerpo;


    public long getId_anuncio() {
        return id_anuncio;
    }

    public void setId_anuncio(long id_anuncio) {
        this.id_anuncio = id_anuncio;
    }

    public long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}

package mingeso.payload.request;

import java.util.Set;

import javax.validation.constraints.*;
 
public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String nombre;
 
    @NotBlank
    @Size(max = 60)
    @Email
    private String correo;
    
    private Set<String> tipo;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String pass;

    @NotBlank
    @Size(max = 100)
    private String comuna;
  
    public String getNombre() {
        return nombre;
    }
 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
    public String getCorreo() {
        return correo;
    }
 
    public void setCorreo(String correo) {
        this.correo = correo;
    }
 
    public String getPass() {
        return pass;
    }
 
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public Set<String> getTipo() {
      return this.tipo;
    }
    
    public void setTipo(Set<String> tipo) {
      this.tipo = tipo;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

}

package mingeso.payload.response;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String nombre;
	private String correo;
	private List<String> tipo;

	public JwtResponse(String accessToken, Long id, String nombre, String correo, List<String> tipo) {
		this.token = accessToken;
		this.id = id;
		this.nombre = nombre;
		this.correo = correo;
		this.tipo = tipo;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<String> getTipo() {
		return tipo;
	}
}

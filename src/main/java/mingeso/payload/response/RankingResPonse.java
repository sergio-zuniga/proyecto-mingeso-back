package mingeso.payload.response;

import javax.validation.constraints.NotBlank;

public class RankingResPonse {

	private String nombre;


	private Long cantidad;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public RankingResPonse(String nombre, Long cantidad) {
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	public RankingResPonse() {

	}

}

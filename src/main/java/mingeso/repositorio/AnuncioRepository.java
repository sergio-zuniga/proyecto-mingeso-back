package mingeso.repositorio;

import mingeso.modelo.Anuncio;
import mingeso.modelo.Categoria;
import mingeso.modelo.Usuario;
import mingeso.payload.response.RankingResPonse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface AnuncioRepository extends JpaRepository<Anuncio, Long> {
	Optional<Anuncio> findById(String nombre);
	Boolean existsByUsuario(Usuario usuario);
	List<Anuncio> findByUsuario(Usuario usuario);
	Iterable<Anuncio> getAllByCategorias(Categoria categoria);
	Boolean existsByCorreo(String correo);

	@Query("SELECT new mingeso.payload.response.RankingResPonse(u.nombre, COUNT(*)) FROM Anuncio a, Usuario u, Comuna c " +
			" WHERE a.usuario = u AND u.comuna = c AND c.nombre = ?1 group by u.id order by COUNT(*) desc")
	List<RankingResPonse>  obtenerRanking( String comuna);

}
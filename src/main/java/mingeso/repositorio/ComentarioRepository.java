package mingeso.repositorio;

import mingeso.modelo.Anuncio;
import mingeso.modelo.Categoria;
import mingeso.modelo.Comentario;
import mingeso.modelo.Usuario;
import mingeso.payload.response.RankingResPonse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ComentarioRepository extends JpaRepository<Comentario, Long> {
    Iterable<Comentario> getAllByAnuncio(Anuncio anuncio);
}
package mingeso.repositorio;


import mingeso.modelo.Comuna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ComunaRepository extends JpaRepository<Comuna, Long> {
    Boolean existsByNombre(String nombre);
    Comuna getFirstByNombre(String nombre);
}
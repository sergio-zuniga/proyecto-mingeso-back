package mingeso.repositorio;

import mingeso.modelo.NombreTipo;
import mingeso.modelo.Tipo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface TipoRepository extends JpaRepository<Tipo, Long> {
	Tipo findByName(NombreTipo name);
}
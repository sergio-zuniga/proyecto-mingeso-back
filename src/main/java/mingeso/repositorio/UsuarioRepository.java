package mingeso.repositorio;

import mingeso.modelo.Usuario;

import org.springframework.stereotype.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	Optional<Usuario> findByCorreo(String nombre);
	Usuario getFirstById(Long id);
	Boolean existsByCorreo(String correo);
	boolean existsById(Long id);


}
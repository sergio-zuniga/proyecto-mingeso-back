package mingeso;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mingeso.Controlador.AnuncioController;
import mingeso.Controlador.AuthController;
import mingeso.Controlador.CarreraRepository;
import mingeso.Controlador.ControladorPrincipal;
import mingeso.Dto.modelo.AnuncioDTO;
import mingeso.Dto.modelo.ComentarioDTO;
import mingeso.Dto.modelo.UsuarioDTO;
import mingeso.modelo.*;
import mingeso.payload.request.AnuncioRequest;
import mingeso.payload.request.ComentarioRequest;
import mingeso.payload.request.LoginRequest;
import mingeso.payload.request.SignupRequest;
import mingeso.payload.response.JwtResponse;
import mingeso.payload.response.MessageResponse;
import mingeso.payload.response.RankingResPonse;
import mingeso.repositorio.AnuncioRepository;
import mingeso.repositorio.CategoriaRepository;
import mingeso.repositorio.UsuarioRepository;
import mingeso.seguridad.jwt.JwtUtils;
import mingeso.seguridad.services.UserDetailsImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import mingeso.modelo.Usuario;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import java.util.*;

import static org.junit.Assert.*;

//import org.junit.Test;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:db-test.properties")
@SpringBootTest
@AutoConfigureMockMvc
class MingesoApplicationTests {

	@Test
	void contextLoads() {
	}
    /*
	@Test
	public void testAppHasAGreeting() {
		App classUnderTest = new App();
		assertNotNull("app should have a greeting", classUnderTest.getGreeting());
	}
    */
	@Autowired
    AnuncioRepository anuncioRepository;
	@Autowired
    CategoriaRepository categoriaRepository;
	@Autowired
    CarreraRepository carreraRepository;
	@Autowired
    UsuarioRepository usuarioRepository;



    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectmapper;




	@Test
	public void testRepo() {
		assertNotNull(anuncioRepository.findAll());
		assertNotNull(categoriaRepository.findAll());
		assertNotNull(usuarioRepository.findAll());
		assertNotNull(carreraRepository.findAll());
	}

	@Test
    public void testInsertUsuario(){
	    Usuario us = new Usuario("nombre", "correo@algo.com", "password");
        us.setId((long) 1);
	    usuarioRepository.save(us);

        Optional<Usuario> found = usuarioRepository.findById(us.getId());
        assertNotNull(found);
    }

    @Test
    public void testCategoria() {
        Categoria categoria = new Categoria("categoria");
        assertNotNull(categoria.getNombre());
        categoria.setNombre("nombre2");
        categoria.setId((long) 1);
        assertNotNull(categoria.getNombre());
        assertNotNull(categoria.getId());
    }

    @Test
    public void testInsertCategoria(){
	    Categoria categoria= new Categoria("categoria");
	    categoria.setId((long) 1);
	    categoriaRepository.save(categoria);
        Optional<Categoria> found = categoriaRepository.findById(categoria.getId());
        assertNotNull(found);

    }

    //ojito con esta que aveces falla no se por que :c
    @Test
    public void testTipo(){
        NombreTipo np = null;
	    Tipo tipo = new Tipo();
	    tipo.setId(1);
	    tipo.getId();
	    tipo.setName(np);
	    Tipo tip = new Tipo (np);
        assertNotNull(tipo.getId());
	    assertNull(tipo.getName());
    }

    @Test
    public void testAlumno() {
        Carrera carrera = new Carrera("Ingenieria Civil");
        Alumno alumno = new Alumno("11111-1","Juan Perez","2000-02-01",carrera);
        Alumno al =new Alumno();
        al.setNacimiento("2000-02-02");
        al.setNombre("nombre algo");
        al.setRut("1111111-2");
        assertNotNull(alumno.getNacimiento());
        assertNotNull(alumno.getNombre());
        assertNotNull(alumno.getRut());
        assertNotNull(alumno.getCarrera());
    }

    //testeado correctamente
    @Test
    public void testCarrera (){
        Carrera carrera= new Carrera("carrera");
        assertNotNull(carrera);
        carrera.setId(1);
        carrera.setNombre("nombre nuevo");
        assertNotNull(carrera.getId());
        assertNotNull(carrera.getNombre());
    }

    //testeado correctamente
    @Test
    public void testUsuarioDTO(){
        UsuarioDTO uDTO = new UsuarioDTO("nombre", "correo@gmail");
        assertNotNull(uDTO.getNombre());
        assertNotNull(uDTO.getCorreo());
        assertNotNull(uDTO.getId());
        UsuarioDTO userD=new UsuarioDTO();
        assertNotNull(userD);
    }

    //testeado correctamente
    @Test
    public void testAnuncioDTO(){
        AnuncioDTO anunDTO = new AnuncioDTO("titulo", "correo@gmail.com","12345456", "descripcion del producto", "10-12-2020", "nombre", (float) 1000);
        Set<Categoria> cat = new HashSet<>();
        Categoria categoria = new Categoria();
        cat.add(categoria);
        anunDTO.setCategorias(cat);
        anunDTO.setCorreo("correo2@calgo.com");
        anunDTO.setDescripcion("descrip 2");
        anunDTO.setFecha("2020-12-12");
        anunDTO.setId((long)1);
        assertNotNull(anunDTO.getId());
        anunDTO.setNombre("nombre");
        anunDTO.setPrecio((float)1001);
        anunDTO.setTelefono("89238921");
        anunDTO.setTitulo("titulo2");
        assertNotNull(anunDTO.getCorreo());
        assertNotNull(anunDTO.getFecha());
        assertNotNull(anunDTO.getDescripcion());
        assertNotNull(anunDTO.getNombre());
        assertNotNull(anunDTO.getTelefono());
        assertNotNull(anunDTO.getTitulo());
        assertNotNull(anunDTO.getPrecio());
        assertNotNull(anunDTO.getCategorias());
        AnuncioDTO dto = new AnuncioDTO();
        assertNotNull(dto);
    }

    @Test
    public void testAnuncioRequest(){
	    Set<String> categorias =  new HashSet<>();
	    categorias.add("categoria 1");
        AnuncioRequest ar = new AnuncioRequest();
        ar.setCategorias(categorias);
        ar.setCorreo("correo@alog.com");
        ar.setDescripcion("descripcion");
        ar.setId_usuario(1);
        ar.setPrecio(1000);
        ar.setTelefono("198287423");
        ar.setTitulo("titulo bkn");
        assertNotNull(ar.getCategorias());
        assertNotNull(ar.getCorreo());
        assertNotNull(ar.getDescripcion());
        assertNotNull(ar.getPrecio());
        assertNotNull(ar.getId_usuario());
        assertNotNull(ar.getTelefono());
        assertNotNull(ar.getTitulo());
    }

    @Test
    public void testLoginRequest(){
	    LoginRequest lr = new LoginRequest();
	    lr.setCorreo("correo@algo.com");
	    lr.setPass("password");
	    assertNotNull(lr.getCorreo());
	    assertNotNull(lr.getPass());
    }
    @Test
    public void testSignupRequest(){
        SignupRequest sr =  new SignupRequest();
        sr.setCorreo("correo");
        sr.setNombre("nombre");
        sr.setPass("password");
        Set<String> tipo = new HashSet<>();
        tipo.add("tipo");
        sr.setTipo(tipo);
        assertNotNull(sr.getCorreo());
        assertNotNull(sr.getNombre());
        assertNotNull(sr.getPass());
        assertNotNull(sr.getTipo());
    }
/*
    @Test
    public void testInsertarAnuncio() {
        Usuario usuario = new Usuario("nombre", "correo@gmail.com","password");
        Categoria categoria= new Categoria("categoria");
        Set <Categoria> categorias = new HashSet<>();
        categorias.add(categoria);
        Anuncio anuncio = new Anuncio("titulo","correo@gmail.com","0912345678","descripcion", (float)1000, usuario, categorias );
        anuncio.setId((long)1);
        anuncioRepository.save(anuncio);
        Optional<Anuncio> found = anuncioRepository.findById(anuncio.getId());
        assertNotNull(found);
    }
    @Test
    public void testCosas(){

    }*/
    @Test
    public void testJwtUtils(){
        JwtUtils ju = new JwtUtils();
        assertFalse(ju.validateJwtToken("token falso"));

        //falta un token de verdad :c
    }

    @Test
    public void testAnuncio(){
        Usuario usuario = new Usuario("nombre", "correo@gmail.com","password");
        Set<Categoria> categorias = new HashSet<>();
        Set<Categoria> categorias2 = new HashSet<>();
        Anuncio anuncio = new Anuncio("titulo","correo@gmail.com","0912345678","descripcion", 1000, usuario, categorias );
        assertNotNull(anuncio.getCorreo());
        assertNotNull(anuncio.getDescripcion());
        assertNotNull((anuncio.getTelefono()));
        assertNotNull(anuncio.getTitulo());
        assertNotNull(anuncio.getPrecio());
        assertNotNull(anuncio.getUsuario());
        assertNotNull(anuncio.getCategorias());
        /*se repite el proceso para testear los setters de la clase Anuncio*/
        anuncio.setCategorias(categorias2);
        anuncio.setCorreo("correo@gmail.com");
        anuncio.setDescripcion("descripcion2");
        anuncio.setFecha("10-10-20");
        anuncio.setPrecio((float)1000);
        anuncio.setUsuario(usuario);
        anuncio.setTitulo("titulo2");
        anuncio.setTelefono("892879329");
        anuncio.setId((long) 1);
        assertNotNull(anuncio.getFecha());
        assertNotNull(anuncio.getId());
        assertNotNull(anuncio.getCorreo());
        assertNotNull(anuncio.getDescripcion());
        assertNotNull((anuncio.getTelefono()));
        assertNotNull(anuncio.getTitulo());
        assertNotNull(anuncio.getPrecio());
        assertNotNull(anuncio.getUsuario());
        assertNotNull(anuncio.getCategorias());

    }
    @Test
    public void testUsuario(){
        Usuario usuario = new Usuario("nombre", "correo@gmail.com","password");
        assertNotNull(usuario.getCorreo());
        assertNotNull(usuario.getNombre());
        assertNotNull(usuario.getPass());
        Usuario us = new Usuario();
        us.setCorreo("correo@algo");
        us.setNombre("nombreII");
        us.setPass("contrasena2");
        Set<Tipo> tipos = new HashSet<>();
        us.setTipos(tipos);
        Comuna comuna = new Comuna("comuna");
        us.setComuna(comuna);
        assertNotNull(us.getComuna());
        assertNotNull(us.getPass());
        assertNotNull(us.getNombre());
        assertNotNull(us.getCorreo());
        assertNotNull(us.getTipos());

    }
    @Test
    public void testMessage(){
        MessageResponse msg = new MessageResponse("mensaje");
        assertNotNull(msg.getMessage());
        msg.setMessage("mensaje nuevo");
        assertNotNull(msg.getMessage());
    }

    @Test
    public void testIntermedia(){
        Intermedia inter = new Intermedia("12345678-9",(long)10);
        assertNotNull(inter.getRut());
        assertNotNull(inter.getId());
        inter.setId((long)1);
        inter.setRut("102398032-9");
        assertNotNull(inter.getId());
        assertNotNull(inter.getRut());

    }

    @Test
    public void testJwtResponse(){
        List<String> tipo = new ArrayList<String>();
        tipo.add("uno");
        JwtResponse jr = new JwtResponse("token", (long) 1, "nombre","correo@algo.com", tipo);
        assertNotNull(jr.getAccessToken());
        assertNotNull(jr.getCorreo());
        assertNotNull(jr.getTipo());
        assertNotNull(jr.getNombre());
        jr.setId((long)1);
        jr.setCorreo("correo2");
        jr.setAccessToken("token2");
        jr.setTokenType("token type");
        jr.setNombre("nombre2");
        jr.setAccessToken("toker generico");
        assertNotNull(jr.getAccessToken());
        assertNotNull(jr.getTokenType());
        assertNotNull(jr.getId());

    }

    @Test
    public void testUserDetailImpl(){
        Collection<? extends GrantedAuthority> authorities = null;
        UserDetailsImpl udi = new UserDetailsImpl((long)1,"nombre", "correo@algo.com", "contrasena", authorities);
        assertNotNull( udi.getId());
        assertNotNull( udi.getNombre());
        assertNotNull(udi.getUsername());
        assertNotNull(udi.getPassword());
        assertNull(authorities);
        assertTrue(udi.isAccountNonExpired());
        assertTrue(udi.isAccountNonLocked());
        assertTrue(udi.isCredentialsNonExpired());
        assertTrue(udi.isEnabled());
        Usuario us =  new Usuario();
        assertTrue(udi.equals(udi));
        assertFalse(udi.equals(us));
        assertFalse(udi.equals(null));
        assertNull(udi.getAuthorities());
        us.setId((long)1);
        us.setPass("pass");
        us.setNombre("nombre");
        us.setCorreo("correo");
        Tipo tipo = new Tipo();
        NombreTipo np = null;
        tipo.setName(np);
        tipo.setId(1);
        Set<Tipo> tipos= new HashSet<>();
        tipos.add(tipo);
        us.setTipos(tipos);
        assertFalse(udi.equals(us));
        //UserDetailsImpl use= (UserDetailsImpl) us;
        //assertNotNull(use);
    }

    @Test
    public  void testRankingResponse(){
        RankingResPonse rr = new RankingResPonse();
        RankingResPonse rr2= new RankingResPonse("nombre ", (long)100);
        rr.setCantidad((long)1000);
        rr.setNombre("nombre cosa");
        assertNotNull(rr2);
        assertNotNull(rr.getCantidad());
        assertNotNull(rr.getNombre());

    }

    @Test
    public void testComentarioDTO(){
        ComentarioDTO cdto = new ComentarioDTO((long)1, "cuerpo", "2020-01-01", "usuario");
        ComentarioDTO cdto2= new ComentarioDTO();
        cdto2.setId((long)1);
        cdto2.setCuerpo("cuerpo2");
        cdto2.setFecha("2020-02-02");
        cdto2.setUsuario("user2");
        assertNotNull(cdto);
        assertNotNull(cdto2.getFecha());
        assertNotNull(cdto2.getCuerpo());
        assertNotNull(cdto2.getId());
        assertNotNull(cdto2.getUsuario());
    }
    @Test
    public void testComuna(){
        Comuna comuna = new Comuna();
        Comuna com= new Comuna("nombre");
        comuna.setId((long)1);
        comuna.setNombre("pudahuel");
        assertNotNull(comuna.getId());
        assertNotNull(comuna.getNombre());
        assertNotNull(com.getNombre());
    }
    @Test
    public void testComentarioRequest(){
        ComentarioRequest cr = new ComentarioRequest();
        cr.setCuerpo("cuerpo");
        cr.setId_anuncio((long)1);
        cr.setId_usuario((long)1);
        assertNotNull(cr.getId_usuario());
        assertNotNull(cr.getCuerpo());
        assertNotNull(cr.getId_anuncio());
    }

    @Test
    public void testComentario(){
        Usuario us =  new Usuario();
        Anuncio an = new Anuncio();
        Comentario com = new Comentario("cuerpo", us,an );
        Comentario comentario = new Comentario();
        comentario.setId((long)1);
        comentario.setAnuncio(an);
        comentario.setCuerpo("cuerpo 2");
        comentario.setFecha("yyyy-MM-dd HH:mm:ss");
        comentario.setUsuario(us);
        assertNotNull(com);
        assertNotNull(comentario.getAnuncio());
        assertNotNull(comentario.getId());
        assertNotNull(comentario.getCuerpo());
        assertNotNull(comentario.getFecha());
        assertNotNull(comentario.getUsuario());
    }

    @Test
    void registro() throws Exception {
        SignupRequest usuario = new SignupRequest();
        usuario.setCorreo("usuario@test.com");
        usuario.setNombre("usuariotest");
        usuario.setPass("pass123");
        Set<String> tipo = new HashSet<>(Arrays.asList("municipalidad","representante"));
        usuario.setTipo(tipo);
        mockMvc.perform(post("/api/autenticacion/registrar")
                .contentType("application/json")
                .content(objectmapper.writeValueAsString(usuario)))
                .andExpect(status().isBadRequest());

    }


    @Test
    void inicio() throws Exception{
        LoginRequest usuarioLogin=new LoginRequest();
        usuarioLogin.setCorreo("usuario@test.com");
        usuarioLogin.setPass("pass123");

        mockMvc.perform(post("/api/autenticacion/iniciar")
                .contentType("application/json")
                .content(objectmapper.writeValueAsString(usuarioLogin)))
                .andExpect(status().isOk());
    }

    //revisar si en sonarqube detecta este test
    @Test
    void testTestController() throws Exception {
        assertNotNull(mockMvc.perform(get("/api/all")));
        assertNotNull(mockMvc.perform(get("/api/vecino")));
        assertNotNull(mockMvc.perform(get("/api/muni")));
        assertNotNull(mockMvc.perform(get("/api/admin")));
    }

    @Test
    void obtenerAnuncios() throws Exception{

        mockMvc.perform(get("/api/anuncios/all_anuncio")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void obtenerAnunciosPorCategoria() throws Exception{

        mockMvc.perform(get("/api/anuncios/por_categoria?categoria=Alimentos")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void obtenerComentarios() throws Exception{
        mockMvc.perform(get("/api/anuncios/por_categoria?categoria=Alimentos/1/comentarios")
                .contentType("application/json"))
                .andExpect(status().isOk());
        assertNotNull(mockMvc.perform(get("/api/comentarios")));

    }

    @Test
    void crearAnuncioControllerTest() throws Exception {
        AnuncioRequest anuncio = new AnuncioRequest();
        Usuario user = usuarioRepository.findByCorreo("usuario@test.com").get();

        Set<String> categorias = new HashSet<>(Arrays.asList("testc","test2"));
        anuncio.setCategorias(categorias);
        anuncio.setCorreo("usuario@test.com");
        anuncio.setDescripcion("una descripcion");
        anuncio.setId_usuario(user.getId());
        anuncio.setPrecio(111);
        anuncio.setTelefono("1234567");
        anuncio.setTitulo("titulo");

        mockMvc.perform(post("/api/anuncios/publicar")
                .contentType("application/json")
                .content(objectmapper.writeValueAsString(anuncio)))
                .andExpect(status().isOk());
    }

    //public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    @After
    public void cleanUp() {
        carreraRepository.deleteAll();
        anuncioRepository.deleteAll();
        categoriaRepository.deleteAll();
        usuarioRepository.deleteAll();
    }




}